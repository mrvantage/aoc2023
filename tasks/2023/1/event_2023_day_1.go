package main

import (
	"flag"
	"fmt"
	"log"
	"slices"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

var words map[string]int32 = map[string]int32{
	"one":   1,
	"two":   2,
	"three": 3,
	"four":  4,
	"five":  5,
	"six":   6,
	"seven": 7,
	"eight": 8,
	"nine":  9,
}

func getFirstDigit(input []byte) (result int32, index int, err error) {
	for i, b := range input {
		if rune(47) < rune(b) && rune(b) < rune(58) {
			result = rune(b) - rune(48)
			index = i
			return
		}
	}
	err = fmt.Errorf("no digit found")
	return
}

func getFirstDigitWord(input []byte) (result int32, index int, err error) {

	index = len(input) + 1

	for word, val := range words {
		i := strings.Index(string(input), word)
		if i > -1 && i < index {
			index = i
			result = val
		}
	}

	return
}

func getLastDigit(input []byte) (result int32, index int, err error) {
	inputRevStr := string(input)
	inputRev := []byte(inputRevStr)
	slices.Reverse(inputRev)
	result, index, err = getFirstDigit(inputRev)
	index = len(input) - index
	return
}

func getLastDigitWord(input []byte) (result int32, index int, err error) {

	index = -1

	for word, val := range words {
		s := string(input)
		i := strings.LastIndex(s, word)
		if i > index {
			index = i
			result = val
		}
	}
	return
}

func ProcessLinePart1(input []byte) (result int32, err error) {
	var first, last int32
	first, _, err = getFirstDigit(input)
	if err != nil {
		return
	}

	last, _, err = getLastDigit(input)
	if err != nil {
		return
	}

	result = (first * 10) + last
	return
}

func ProcessLinePart2(input []byte) (result int32, err error) {
	var first, last, fn, ln, fw, lw int32
	var fni, lni, fwi, lwi int
	fn, fni, err = getFirstDigit(input)
	if err != nil {
		fni = len(input) + 1
	}

	fw, fwi, err = getFirstDigitWord(input)
	if err != nil {
		fwi = len(input) + 1
	}

	if fni < fwi {
		first = fn
	} else {
		first = fw
	}

	ln, lni, err = getLastDigit(input)
	if err != nil {
		lni = -1
	}

	lw, lwi, err = getLastDigitWord(input)
	if err != nil {
		lwi = -1
	}

	if lni > lwi {
		last = ln
	} else {
		last = lw
	}

	result = (first * 10) + last
	return
}

func Event2023Day1Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	var result int32
	lines := strings.Split(task.Input, "\n")
	for i, line := range lines {

		l, err := ProcessLinePart1([]byte(line))
		if err != nil {
			log.Printf("Warning, parsing line %d: %s", i+1, err.Error())
			continue
		}

		result += l
	}

	log.Printf("Result = %d", result)

	return int(result), nil
}

func Event2023Day1Part2(task *aoc.Task) (int, error) {

	log.Printf("Length of task input is %d", len(task.Input))

	var result int32
	lines := strings.Split(task.Input, "\n")
	for i, line := range lines {

		l, err := ProcessLinePart2([]byte(line))
		if err != nil {
			log.Printf("Warning, parsing line %d: %s", i+1, err.Error())
			continue
		}

		fmt.Printf("%s - %d\n", line, l)

		result += l
	}

	log.Printf("Result = %d", result)

	return int(result), nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 1)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day1Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day1Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
