package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Draw struct {
	Red, Green, Blue int
}

func (r *Draw) Parse(input string) (err error) {
	parts := strings.Split(input, ", ")

	for _, part := range parts {
		res := strings.Split(part, " ")
		if len(res) != 2 {
			err = fmt.Errorf("failed parsing random draw: part `%s` not in expected format", part)
			return
		}

		var val int
		val, err = strconv.Atoi(res[0])
		if err != nil {
			err = fmt.Errorf("failed parsing random draw: %s", err.Error())
			return
		}

		switch res[1] {
		case "red":
			r.Red = val
		case "green":
			r.Green = val
		case "blue":
			r.Blue = val
		default:
			err = fmt.Errorf("failed parsing random draw: invalid color %s", res[1])
			return
		}

	}
	return
}

func NewDraw(input string) (d *Draw, err error) {
	d = &Draw{}
	err = d.Parse(input)
	return
}

type Game struct {
	ID    int
	Draws []*Draw
}

func (g *Game) Parse(input string) (err error) {

	IDSplit := strings.Split(input, ": ")
	if len(IDSplit) != 2 {
		err = fmt.Errorf("failed parsing game: game `%s` not in expected format", input)
		return
	}

	id := strings.Split(IDSplit[0], " ")
	if len(id) != 2 {
		err = fmt.Errorf("failed parsing game: id `%s` not in expected format", IDSplit[0])
		return
	}

	g.ID, err = strconv.Atoi(id[1])
	if err != nil {
		err = fmt.Errorf("failed parsing game: %s", err.Error())
		return
	}

	draws := strings.Split(IDSplit[1], "; ")

	for _, draw := range draws {
		var d *Draw
		d, err = NewDraw(draw)
		if err != nil {
			err = fmt.Errorf("failed parsing game: %s", err.Error())
			return
		}

		g.Draws = append(g.Draws, d)
	}
	return
}

func (g *Game) MaxDraw() (red, green, blue int) {
	for _, d := range g.Draws {
		if d.Red > red {
			red = d.Red
		}
		if d.Green > green {
			green = d.Green
		}
		if d.Blue > blue {
			blue = d.Blue
		}
	}
	return
}

func (g *Game) IsPossible(red, green, blue int) (res bool) {
	maxRed, maxGreen, maxBlue := g.MaxDraw()
	if maxRed <= red && maxGreen <= green && maxBlue <= blue {
		res = true
	}
	return
}

func NewGame(input string) (g *Game, err error) {
	g = &Game{}
	g.Draws = make([]*Draw, 0)
	err = g.Parse(input)
	return
}

type Day2 struct {
	Games []*Game
}

func (d *Day2) Parse(input string) (err error) {

	for i, line := range strings.Split(input, "\n") {
		if len(line) > 0 {
			var g *Game
			g, err = NewGame(line)
			if err != nil {
				err = fmt.Errorf("error parsing line %d: %s", i, err.Error())
				return
			}

			d.Games = append(d.Games, g)
		}
	}
	return
}

func (d Day2) Part1(red, green, blue int) (res int) {
	for _, g := range d.Games {
		if g.IsPossible(red, green, blue) {
			res += g.ID
		}
	}
	return
}

func (d Day2) Part2() (res int) {
	for _, g := range d.Games {
		red, green, blue := g.MaxDraw()
		res += red * green * blue
	}
	return
}

func Event2023Day2Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	d := Day2{}
	d.Games = make([]*Game, 0)

	err := d.Parse(task.Input)
	if err != nil {
		return 0, err
	}

	return d.Part1(12, 13, 14), nil
}

func Event2023Day2Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	d := Day2{}
	d.Games = make([]*Game, 0)

	err := d.Parse(task.Input)
	if err != nil {
		return 0, err
	}

	return d.Part2(), nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 2)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day2Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day2Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
