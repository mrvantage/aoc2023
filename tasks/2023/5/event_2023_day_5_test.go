package main

import (
	"reflect"
	"testing"
)

func TestRange_OverlappingRange(t *testing.T) {
	type fields struct {
		Start  int
		Length int
		Parent *Range
	}
	type args struct {
		in Range
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantOut Range
	}{
		{
			name: "in shifted right",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  5,
					Length: 10,
				},
			},
			wantOut: Range{
				Start:  5,
				Length: 5,
			},
		},
		{
			name: "in shifted left",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  -5,
					Length: 10,
				},
			},
			wantOut: Range{
				Start:  0,
				Length: 5,
			},
		},
		{
			name: "in outside range to the right",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  15,
					Length: 10,
				},
			},
			wantOut: Range{
				Start:  0,
				Length: 0,
			},
		},
		{
			name: "in outside range to the left",
			fields: fields{
				Start:  15,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  0,
					Length: 10,
				},
			},
			wantOut: Range{
				Start:  0,
				Length: 0,
			},
		},
		{
			name: "in within range",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  2,
					Length: 5,
				},
			},
			wantOut: Range{
				Start:  2,
				Length: 5,
			},
		},
		{
			name: "range within in",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  -10,
					Length: 30,
				},
			},
			wantOut: Range{
				Start:  0,
				Length: 10,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := Range{
				Start:  tt.fields.Start,
				Length: tt.fields.Length,
				Parent: tt.fields.Parent,
			}
			if gotOut := r.OverlappingRange(tt.args.in); !reflect.DeepEqual(gotOut, tt.wantOut) {
				t.Errorf("Range.OverlappingRange() = %v, want %v", gotOut, tt.wantOut)
			}
		})
	}
}

func TestRange_OutsideRange(t *testing.T) {
	type fields struct {
		Start  int
		Length int
		Parent *Range
	}
	type args struct {
		in Range
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantOut []Range
	}{
		{
			name: "in shifted right",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  5,
					Length: 10,
				},
			},
			wantOut: []Range{
				{
					Start:  10,
					Length: 5,
				},
			},
		},
		{
			name: "in shifted left",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  -5,
					Length: 10,
				},
			},
			wantOut: []Range{
				{
					Start:  -5,
					Length: 5,
				},
			},
		},
		{
			name: "range within in",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  -10,
					Length: 30,
				},
			},
			wantOut: []Range{
				{
					Start:  -10,
					Length: 10,
				},
				{
					Start:  10,
					Length: 10,
				},
			},
		},
		{
			name: "in within range",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  2,
					Length: 5,
				},
			},
			wantOut: []Range{},
		},
		{
			name: "in shifted left - one overlap",
			fields: fields{
				Start:  56,
				Length: 37,
			},
			args: args{
				in: Range{
					Start:  46,
					Length: 11,
				},
			},
			wantOut: []Range{
				{
					Start:  46,
					Length: 10,
				},
			},
		},
		{
			name: "in shifted right - one overlap",
			fields: fields{
				Start:  46,
				Length: 11,
			},
			args: args{
				in: Range{
					Start:  56,
					Length: 37,
				},
			},
			wantOut: []Range{
				{
					Start:  57,
					Length: 36,
				},
			},
		},
		{
			name: "in shifted left - same end",
			fields: fields{
				Start:  46,
				Length: 11,
			},
			args: args{
				in: Range{
					Start:  42,
					Length: 15,
				},
			},
			wantOut: []Range{
				{
					Start:  42,
					Length: 4,
				},
			},
		},
		{
			name: "in shifted rigth - same start",
			fields: fields{
				Start:  46,
				Length: 11,
			},
			args: args{
				in: Range{
					Start:  46,
					Length: 15,
				},
			},
			wantOut: []Range{
				{
					Start:  57,
					Length: 4,
				},
			},
		},
		{
			name: "in outside range to the right",
			fields: fields{
				Start:  0,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  15,
					Length: 10,
				},
			},
			wantOut: []Range{
				{
					Start:  15,
					Length: 10,
				},
			},
		},
		{
			name: "in outside range to the left",
			fields: fields{
				Start:  15,
				Length: 10,
			},
			args: args{
				in: Range{
					Start:  0,
					Length: 10,
				},
			},
			wantOut: []Range{
				{
					Start:  0,
					Length: 10,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := Range{
				Start:  tt.fields.Start,
				Length: tt.fields.Length,
				Parent: tt.fields.Parent,
			}
			if gotOut := r.OutsideRange(tt.args.in); !reflect.DeepEqual(gotOut, tt.wantOut) {
				t.Errorf("Range.OutsideRange() = %v, want %v", gotOut, tt.wantOut)
			}
		})
	}
}
