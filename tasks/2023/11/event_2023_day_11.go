package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"slices"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Coordinate struct {
	X int
	Y int
}

func (c Coordinate) Distance(comp Coordinate) (res int) {
	res = int(math.Abs(float64(comp.X)-float64(c.X)) + math.Abs(float64(comp.Y)-float64(c.Y)))
	return
}

func (c Coordinate) DistanceExpanded(comp Coordinate, emptyRows, emptyColumns []int, factor int) (res int) {
	res = int(math.Abs(float64(comp.X)-float64(c.X)) + math.Abs(float64(comp.Y)-float64(c.Y)))

	var startX, endX int
	if c.X < comp.X {
		startX = c.X
		endX = comp.X
	} else {
		startX = comp.X
		endX = c.X
	}
	for i := startX + 1; i < endX; i++ {
		if slices.Contains[[]int, int](emptyColumns, i) {
			res += factor - 1
		}
	}

	var startY, endY int
	if c.Y < comp.Y {
		startY = c.Y
		endY = comp.Y
	} else {
		startY = comp.Y
		endY = c.Y
	}
	for i := startY + 1; i < endY; i++ {
		if slices.Contains[[]int, int](emptyRows, i) {
			res += factor - 1
		}
	}
	return
}

type Universe struct {
	Map [][]bool
}

func (u *Universe) Parse(input string) {
	u.Map = make([][]bool, 0)
	for _, line := range strings.Split(input, "\n") {

		if len(line) > 0 {
			row := make([]bool, 0)

			for _, galaxy := range line {
				if string(galaxy) == "#" {
					row = append(row, true)
				} else {
					row = append(row, false)
				}
			}
			u.Map = append(u.Map, row)
		}
	}
}

func (u Universe) RotateRight() (res Universe) {
	res.Map = make([][]bool, len(u.Map[0]))

	for i := range res.Map {
		res.Map[i] = make([]bool, len(u.Map))
	}

	for y, row := range u.Map {
		for x, g := range row {
			newY := x
			newX := len(u.Map) - y - 1
			res.Map[newY][newX] = g
		}
	}
	return
}

func (u Universe) ExpandVertical() (res Universe) {

	res.Map = make([][]bool, 0)

	for _, row := range u.Map {

		res.Map = append(res.Map, row)
		if !slices.Contains[[]bool, bool](row, true) {
			res.Map = append(res.Map, row)
		}
	}
	return
}

func (u Universe) Expand() (res Universe) {
	res = u.
		ExpandVertical().
		RotateRight().
		ExpandVertical().
		RotateRight().
		RotateRight().
		RotateRight()
	return
}

func (u Universe) GetGalaxyCoords() (res []Coordinate) {
	res = make([]Coordinate, 0)
	for y, row := range u.Map {
		for x, g := range row {
			if g {
				res = append(res, Coordinate{X: x, Y: y})
			}
		}
	}
	return
}

func (u Universe) GetGalaxyPairs() (a, b []Coordinate) {
	a = make([]Coordinate, 0)
	b = make([]Coordinate, 0)
	coords := u.GetGalaxyCoords()

	for i, coord := range coords {
		for j := i + 1; j < len(coords); j++ {
			a = append(a, coord)
			b = append(b, coords[j])
		}
	}
	return
}

func (u Universe) GetEmptyRows() (res []int) {
	res = make([]int, 0)
	for y, row := range u.Map {
		if !slices.Contains[[]bool, bool](row, true) {
			res = append(res, y)
		}
	}
	return
}

func (u Universe) GetEmptyColumns() (res []int) {
	r := u.RotateRight()
	res = r.GetEmptyRows()
	return
}

func Event2023Day11Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	u := Universe{}
	u.Parse(task.Input)

	u = u.Expand()

	a, b := u.GetGalaxyPairs()

	var res int
	for i, c := range a {
		res += c.Distance(b[i])
	}

	return res, nil
}

func Event2023Day11Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	u := Universe{}
	u.Parse(task.Input)

	emptyRows := u.GetEmptyRows()
	emptyColumns := u.GetEmptyColumns()

	a, b := u.GetGalaxyPairs()

	var res int
	for i, c := range a {
		res += c.DistanceExpanded(b[i], emptyRows, emptyColumns, 1000000)
	}

	return res, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 11)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day11Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day11Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
