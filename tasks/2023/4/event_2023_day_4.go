package main

import (
	"flag"
	"fmt"
	"log"
	"slices"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Card struct {
	Number  int
	Winning []int
	Have    []int
}

func (c *Card) Parse(line string) (err error) {

	c.Winning = make([]int, 0)
	c.Have = make([]int, 0)

	split1 := strings.Split(line, ": ")

	split2 := strings.Split(split1[0], " ")
	c.Number, err = strconv.Atoi(split2[len(split2)-1])
	if err != nil {
		return
	}

	split3 := strings.Split(split1[1], " | ")

	split4 := strings.Split(split3[0], " ")

	for _, w := range split4 {
		if n, err := strconv.Atoi(w); err == nil {
			c.Winning = append(c.Winning, n)
		}
	}

	split5 := strings.Split(split3[1], " ")

	for _, w := range split5 {
		if n, err := strconv.Atoi(w); err == nil {
			c.Have = append(c.Have, n)
		}
	}
	return
}

func (c Card) CalculatePoints() (res int) {
	for _, h := range c.Have {
		if slices.Contains[[]int, int](c.Winning, h) {
			if res == 0 {
				res = 1
			} else {
				res = res * 2
			}
		}
	}
	return
}

func (c Card) NumWins() (res int) {
	for _, h := range c.Have {
		if slices.Contains[[]int, int](c.Winning, h) {
			res++
		}
	}
	return
}

func Event2023Day4Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	var total int
	for _, line := range strings.Split(task.Input, "\n") {
		c := Card{}
		c.Parse(line)
		total += c.CalculatePoints()
	}

	return total, nil
}

func Event2023Day4Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	CardsByNumber := make(map[int]Card)
	Cards := make([]Card, 0)
	for _, line := range strings.Split(task.Input, "\n") {
		if len(line) > 0 {
			c := Card{}
			c.Parse(line)
			CardsByNumber[c.Number] = c
			Cards = append(Cards, c)
		}
	}

	Processed := make([]Card, 0)

	for len(Cards) > 0 {
		p := Cards
		Cards = make([]Card, 0)
		for _, c := range p {
			for i := c.Number + 1; i <= c.Number+c.NumWins(); i++ {
				Cards = append(Cards, CardsByNumber[i])
			}

			Processed = append(Processed, c)
		}
	}

	return len(Processed), nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 4)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day4Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day4Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
