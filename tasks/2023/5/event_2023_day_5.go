package main

import (
	"flag"
	"fmt"
	"log"
	"slices"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Range struct {
	Start  int
	Length int
	Parent *Range
}

func (r Range) End() (res int) {
	res = r.Start + r.Length - 1
	return
}

func (r Range) OverlappingRange(in Range) (out Range) {
	if r.Start > in.Start {
		out.Start = r.Start
	} else {
		out.Start = in.Start
	}

	if r.End() < in.End() {
		out.Length = r.End() - out.Start + 1
	} else {
		out.Length = in.End() - out.Start + 1
	}

	if in.Start > r.End() || in.End() < r.Start {
		out = Range{Start: 0, Length: 0}
	}

	return
}

func (r Range) OutsideRange(in Range) (out []Range) {
	out = make([]Range, 0)

	if r.Start > in.Start && in.End() >= r.Start {
		out = append(out, Range{Start: in.Start, Length: r.Start - in.Start})
	}

	if r.End() < in.End() && in.Start <= r.End() {
		out = append(out, Range{Start: r.End() + 1, Length: in.End() - r.End()})
	}

	if in.Start > r.End() || in.End() < r.Start {
		out = append(out, in)
	}

	return
}

func (r Range) String() string {
	return fmt.Sprintf("%d,|%d|,%d", r.Start, r.Length, r.End())
}

type Converter struct {
	Type   string
	Output int
	Range  Range
}

func (c Converter) Convert(in int) (out int, converted bool) {
	if in >= c.Range.Start && in < c.Range.End() {
		out = in + (c.Output - c.Range.Start)
		converted = true
	} else {
		out = in
	}
	return
}

func (c Converter) ConvertRange(in Range) (out Range, converted bool) {

	out = c.Range.OverlappingRange(in)

	if out.Length > 0 {
		out.Start = out.Start + (c.Output - c.Range.Start)
		converted = true
	} else {
		out = in
	}
	return
}

func (c Converter) String() string {
	return fmt.Sprintf("%s %d %s", c.Type, c.Output, c.Range)
}

func Event2023Day5Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	parts := strings.Split(task.Input, "\n\n")

	converters := make([][]Converter, len(parts[1:]))
	for i, part := range parts[1:] {
		lines := strings.Split(part, "\n")
		converterType := lines[0]

		converters[i] = make([]Converter, 0)

		for _, c := range lines[1:] {
			if len(c) > 0 {
				vars := strings.Split(c, " ")

				output, err := strconv.Atoi(vars[0])
				if err != nil {
					log.Printf("Could not convert %s to int: %s", vars[0], err.Error())
				}

				start, err := strconv.Atoi(vars[1])
				if err != nil {
					log.Printf("Could not convert %s to int: %s", vars[1], err.Error())
				}

				length, err := strconv.Atoi(vars[2])
				if err != nil {
					log.Printf("Could not convert %s to int: %s", vars[2], err.Error())
				}

				converters[i] = append(converters[i], Converter{Type: converterType, Output: output, Range: Range{Start: start, Length: length}})
			}
		}

	}

	locations := make([]int, 0)
	for _, seed := range strings.Split(parts[0], " ")[1:] {

		seedInt, err := strconv.Atoi(seed)
		if err != nil {
			log.Printf("Could not convert %s to int: %s", seed, err.Error())
		}

		val := seedInt
		for _, typeConverters := range converters {
			converted := false
			for _, converter := range typeConverters {
				val, converted = converter.Convert(val)

				if converted {
					break
				}
			}

		}

		locations = append(locations, val)
	}

	res := slices.Min[[]int, int](locations)

	return res, nil
}

func Event2023Day5Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	parts := strings.Split(task.Input, "\n\n")

	typeConverters := make([][]Converter, len(parts[1:]))
	for i, part := range parts[1:] {
		lines := strings.Split(part, "\n")
		converterType := lines[0]

		typeConverters[i] = make([]Converter, 0)

		for _, c := range lines[1:] {
			if len(c) > 0 {
				vars := strings.Split(c, " ")

				output, err := strconv.Atoi(vars[0])
				if err != nil {
					log.Printf("Could not convert 1 %s to int: %s", vars[0], err.Error())
				}

				start, err := strconv.Atoi(vars[1])
				if err != nil {
					log.Printf("Could not convert 2 %s to int: %s", vars[1], err.Error())
				}

				length, err := strconv.Atoi(vars[2])
				if err != nil {
					log.Printf("Could not convert 3 %s to int: %s", vars[2], err.Error())
				}

				typeConverters[i] = append(typeConverters[i], Converter{Type: converterType, Output: output, Range: Range{Start: start, Length: length}})
			}
		}

	}

	seedInput := strings.Split(parts[0], " ")

	nextTypeRanges := make([]Range, 0)

	for i := 1; i < len(seedInput[1:]); i = i + 2 {

		start, err := strconv.Atoi(seedInput[i])
		if err != nil {
			log.Printf("Could not convert 4 %s to int: %s", parts[i], err.Error())
		}

		num, err := strconv.Atoi(seedInput[i+1])
		if err != nil {
			log.Printf("Could not convert 5 %s to int: %s", parts[i+1], err.Error())
		}

		nextTypeRanges = append(nextTypeRanges, Range{Start: start, Length: num})

	}

	// Parsing up to here, this is where the magic happens....

	// Start by looping through the layers of levels of converters
	for _, converters := range typeConverters {

		// Copy the seed ranges for current level type from the previous level type (or parsing for first level type)
		currentTypeRanges := nextTypeRanges
		// Reset slice for next level type, to track new and existing ranges for next level
		nextTypeRanges = make([]Range, 0)

		// loop trough ranges for current level
		for _, cur := range currentTypeRanges {

			// Set initial nextConverterRanges to bootstrap for this range on this level
			nextConverterRanges := []Range{cur}

			// Loop through converters for this level
			for _, conv := range converters {

				// Copy ranges output from previous converter (or take the bootstrapped one) as input for current converter
				currentConverterRanges := nextConverterRanges
				// Reset slice for next converter, to track new and existing ranges for next converter
				nextConverterRanges = make([]Range, 0)

				// Loop through input ranges for current converter
				for _, item := range currentConverterRanges {

					// Try to convert the range
					n, converted := conv.ConvertRange(item)
					if converted {
						// if conversion was successful, pass result to next level type
						nextTypeRanges = append(nextTypeRanges, n)
					}

					// Add parts of input that did not match to input for next converter
					nextConverterRanges = append(nextConverterRanges, conv.Range.OutsideRange(item)...)
				}
			}

			// Since we've looped through all convertes, pass all that didn't match to next level type
			nextTypeRanges = append(nextTypeRanges, nextConverterRanges...)

		}

	}

	// Once handled all levels, loop through all output to find the lowest value
	var lowest int
	for _, r := range nextTypeRanges {
		if r.Start < lowest || lowest == 0 {
			lowest = r.Start
		}

	}

	return lowest, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 5)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day5Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day5Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
