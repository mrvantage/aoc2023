package main

import (
	"flag"
	"fmt"
	"log"
	"slices"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Sequence struct {
	Data []int
}

func (s *Sequence) Parse(line string) (err error) {
	s.Data = make([]int, 0)
	elements := strings.Split(line, " ")

	for _, e := range elements {
		var n int
		n, err = strconv.Atoi(e)
		if err != nil {
			return
		}
		s.Data = append(s.Data, n)
	}
	return
}

func (s Sequence) GetSubSequence() (res Sequence) {
	res.Data = make([]int, 0)
	for i, num := range s.Data[1:] {
		res.Data = append(res.Data, num-s.Data[i])
	}
	return
}

func (s Sequence) IsAllZero() (res bool) {
	t := make([]int, 0)
	t = append(t, s.Data...)
	slices.Sort[[]int](t)
	if t[0] == 0 && t[len(t)-1] == 0 {
		res = true
	}
	return
}

func (s Sequence) StringLine(level int) (res string) {
	for i := 0; i < level; i++ {
		res += "  "
	}

	for _, n := range s.Data {
		res += fmt.Sprintf("%d   ", n)
	}

	res += "\n"
	return
}

func (s Sequence) String() (res string) {

	res += s.StringLine(0)
	sub := s.GetSubSequence()
	var i int
	for !sub.IsAllZero() {
		i++
		res += sub.StringLine(i)
		sub = sub.GetSubSequence()
	}

	res += sub.StringLine(i + 1)

	return
}

func (s Sequence) Extrapolate() (res int) {

	sub := s.GetSubSequence()

	if sub.IsAllZero() {
		res = s.Data[0]
		return
	}

	res = s.Data[len(s.Data)-1] + sub.Extrapolate()
	return
}

func (s Sequence) ExtrapolatePast() (res int) {

	sub := s.GetSubSequence()

	if sub.IsAllZero() {
		res = s.Data[0]
		return
	}

	res = s.Data[0] - sub.ExtrapolatePast()
	return
}

func Event2023Day9Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	lines := strings.Split(task.Input, "\n")

	var res int
	for _, line := range lines {
		if len(line) > 0 {
			s := Sequence{}
			err := s.Parse(line)
			if err != nil {
				log.Fatalf("unable to parse line %s", line)
			}
			fmt.Println("input:")
			fmt.Println(s)

			add := s.Extrapolate()
			res += add

			log.Printf("%d --> %d: res %d", s.Data[len(s.Data)-1], add, res)

			s.Data = append(s.Data, add)
			fmt.Println("output:")
			fmt.Println(s)
		}
	}

	return res, nil
}

func Event2023Day9Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	lines := strings.Split(task.Input, "\n")

	var res int
	for _, line := range lines {
		if len(line) > 0 {
			s := Sequence{}
			err := s.Parse(line)
			if err != nil {
				log.Fatalf("unable to parse line %s", line)
			}
			fmt.Println("input:")
			fmt.Println(s)

			add := s.ExtrapolatePast()
			res += add

			log.Printf("%d --> %d: res %d", s.Data[0], add, res)

			s.Data = append([]int{add}, s.Data...)
			fmt.Println("output:")
			fmt.Println(s)
		}
	}

	return res, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 9)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day9Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day9Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
