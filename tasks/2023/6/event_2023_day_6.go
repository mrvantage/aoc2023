package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

// traveled distance = 9
// speed = x
// time traveled = 7 - x

// results in following equation
// 9 = x * (7 - x)
// 9 = -x² + 7x
// 0 = -x² + 7x - 9

type Record struct {
	Time     int
	Distance int
}

func (r Record) Speeds() (s1, s2 float64) {

	// implements abc method: -b ± √(b² - 4ac) / 2a
	a := float64(-1)
	b := float64(r.Time)
	c := float64(-r.Distance)

	// b² - 4ac
	D := math.Pow(b, 2) - (4 * a * c)

	// -b ± √(D) / 2a
	s1 = (-b - math.Sqrt(D)) / (2 * a)
	s2 = (-b + math.Sqrt(D)) / (2 * a)
	return
}

func (r Record) NumWins() (res int) {
	s1, s2 := r.Speeds()

	// If first or last is exact integer, we're off by 1, so use Floor() + 1 instead of Ceil() and vice versa
	var first, last int
	if s1 < s2 {
		first = int(math.Floor(s1)) + 1
		last = int(math.Ceil(s2)) - 1
	} else if s2 < s1 {
		first = int(math.Floor(s2)) + 1
		last = int(math.Ceil(s1)) - 1
	} else {
		return
	}

	res = last - first + 1

	return
}

func Event2023Day6Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	times := strings.Fields(strings.Split(task.Input, "\n")[0])
	distances := strings.Fields(strings.Split(task.Input, "\n")[1])

	res := 1
	for i := 1; i < len(times); i++ {

		time, err := strconv.Atoi(times[i])
		if err != nil {
			log.Printf("Could not convert %s to integer: %s", times[i], err.Error())
		}

		distance, err := strconv.Atoi(distances[i])
		if err != nil {
			log.Printf("Could not convert %s to integer: %s", distances[i], err.Error())
		}

		res = res * Record{Time: time, Distance: distance}.NumWins()

	}

	return res, nil
}

func Event2023Day6Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	times := strings.Fields(strings.Split(task.Input, "\n")[0])
	distances := strings.Fields(strings.Split(task.Input, "\n")[1])

	timeStr := strings.Join(times[1:], "")
	time, err := strconv.Atoi(timeStr)
	if err != nil {
		log.Printf("Could not convert %s to integer: %s", timeStr, err.Error())
	}

	distanceStr := strings.Join(distances[1:], "")
	distance, err := strconv.Atoi(distanceStr)
	if err != nil {
		log.Printf("Could not convert %s to integer: %s", distanceStr, err.Error())
	}

	res := Record{Time: time, Distance: distance}.NumWins()

	return res, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 6)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day6Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day6Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
