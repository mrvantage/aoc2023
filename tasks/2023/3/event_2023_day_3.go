package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func isNumber(val rune) (res bool) {
	if rune(47) < rune(val) && rune(val) < rune(58) {
		res = true
	}
	return
}

type SequenceIndex struct {
	Index  int
	Length int
}

type GridLine []rune

func (g GridLine) String() (res string) {
	for _, char := range g {
		res += string(char)
	}
	return
}

func (g GridLine) GetSequenceIndexes() (res []SequenceIndex) {
	res = make([]SequenceIndex, 0)
	index := -1
	length := 0
	for x, val := range g {
		if isNumber(val) {
			// if this is first digit in sequence
			if x-1 > -1 && !isNumber(g[x-1]) || x == 0 {
				index = x
			}

			length++

			// if this is last digit in sequence
			if x+1 < len(g) && !isNumber(g[x+1]) || x+1 == len(g) {
				res = append(res, SequenceIndex{index, length})
				index = -1
				length = 0
			}
		}
	}
	return
}

type Grid []GridLine

func (g Grid) String() (res string) {
	for _, line := range g {
		res += fmt.Sprintf("%s\n", line)
	}
	return
}

func (g Grid) GetSubGridLocations() (res []SubGridLoc) {
	res = make([]SubGridLoc, 0)
	for y, line := range g {
		sequences := line.GetSequenceIndexes()

		for _, s := range sequences {
			res = append(res, SubGridLoc{X: s.Index - 1, Y: y - 1, Width: s.Length + 2, Height: 3})
		}

	}
	return
}

func (g Grid) GetSubGrid(loc SubGridLoc) (res Grid) {
	res = make(Grid, loc.Height)

	for y := 0; y < loc.Height; y++ {
		res[y] = make(GridLine, loc.Width)

		for x := 0; x < loc.Width; x++ {
			orgY := loc.Y + y
			orgX := loc.X + x
			if orgY < 0 || orgX < 0 || orgY >= len(g) || orgX >= len(g[orgY]) {
				res[y][x] = rune([]byte(".")[0])
			} else {
				res[y][x] = g[orgY][orgX]
			}

		}

	}
	return
}

func (g Grid) GetNumbers() (res []Number) {
	res = make([]Number, 0)
	for _, loc := range g.GetSubGridLocations() {
		res = append(res, Number{Loc: loc, Grid: g.GetSubGrid(loc)})
	}
	return
}

func NewGrid(input string) (res Grid) {
	res = make(Grid, 0)
	for _, line := range strings.Split(input, "\n") {
		res = append(res, GridLine(line))
	}
	return
}

type SubGridLoc struct {
	X      int
	Y      int
	Width  int
	Height int
}

type Number struct {
	Loc  SubGridLoc
	Grid Grid
}

func (n Number) GetValue() (res int, err error) {
	str := n.Grid[1][1 : len(n.Grid[1])-1]
	res, err = strconv.Atoi(string(str))
	return
}

func (n Number) String() (res string) {
	val, err := n.GetValue()
	if err != nil {
		res = "#!err#"
		return
	}
	res = fmt.Sprintf("%d", val)
	return
}

func (n Number) IsValid() (res bool) {

	dot := rune([]byte(".")[0])

	for _, char := range n.Grid[0] {
		if char != dot {
			res = true
			return
		}
	}
	for _, char := range n.Grid[2] {
		if char != dot {
			res = true
			return
		}
	}
	if n.Grid[1][0] != dot || n.Grid[1][len(n.Grid[1])-1] != dot {
		res = true
	}
	return
}

func (n Number) GetStarConnections() (res []SubGridLoc) {

	star := rune([]byte("*")[0])

	res = make([]SubGridLoc, 0)

	for x, char := range n.Grid[0] {
		if char == star {
			res = append(res, SubGridLoc{X: n.Loc.X + x, Y: n.Loc.Y, Width: 1, Height: 1})
		}
	}
	for x, char := range n.Grid[2] {
		if char == star {
			res = append(res, SubGridLoc{X: n.Loc.X + x, Y: n.Loc.Y + 2, Width: 1, Height: 1})
		}
	}
	if n.Grid[1][0] == star {
		res = append(res, SubGridLoc{X: n.Loc.X, Y: n.Loc.Y + 1, Width: 1, Height: 1})
	}
	if n.Grid[1][len(n.Grid[1])-1] == star {
		res = append(res, SubGridLoc{X: n.Loc.X + len(n.Grid[1]) - 1, Y: n.Loc.Y + 1, Width: 1, Height: 1})
	}
	return
}

func Event2023Day3Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	g := NewGrid(task.Input)

	res := 0
	for _, n := range g.GetNumbers() {
		if n.IsValid() {

			val, err := n.GetValue()
			if err != nil {
				log.Printf("Warning could not get value for number %s: %s", n.Grid, err)
			}

			res += val
		}
	}

	return res, nil
}

func Event2023Day3Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	g := NewGrid(task.Input)

	connections := make(map[SubGridLoc][]Number, 0)

	for _, n := range g.GetNumbers() {

		for _, s := range n.GetStarConnections() {
			if connections[s] == nil {
				connections[s] = make([]Number, 0)
			}
			connections[s] = append(connections[s], n)
		}
	}

	res := 0
	for _, c := range connections {
		fmt.Println(len(c))

		if len(c) == 2 {
			val1, err := c[0].GetValue()
			if err != nil {
				log.Panicf("Could not get value for number %v", c[0])
			}
			val2, err := c[1].GetValue()
			if err != nil {
				log.Panicf("Could not get value for number %v", c[0])
			}
			res += val1 * val2
		}
	}

	return res, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 3)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day3Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day3Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
