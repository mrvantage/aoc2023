package main

import (
	"flag"
	"fmt"
	"log"
	"regexp"
	"slices"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Node struct {
	Input string
	Left  string
	Right string
}

func (n *Node) Parse(line string) {
	r := regexp.MustCompile(`(\w{3}) = \((\w{3}), (\w{3})\)$`)
	parts := r.FindStringSubmatch(line)
	n.Input = parts[1]
	n.Left = parts[2]
	n.Right = parts[3]
}

type Map map[string]Node

func Event2023Day8Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	lines := strings.Split(task.Input, "\n")

	m := make(Map)
	for _, line := range lines[2:] {
		if len(line) > 0 {
			n := Node{}
			n.Parse(line)
			m[n.Input] = n
		}
	}

	var arrived bool
	var i int
	var count int
	route := strings.Trim(lines[0], "\n")
	position := "AAA"
	for !arrived {
		if string(route[i]) == "L" {
			position = m[position].Left
		} else {
			position = m[position].Right
		}
		count++

		log.Printf("New position %s", position)

		if position == "ZZZ" {
			arrived = true
		}

		i++
		if i == len(route) {
			i = 0
		}
	}

	return count, nil
}

func Event2023Day8Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	lines := strings.Split(task.Input, "\n")

	m := make(Map)
	positions := make([]Node, 0)

	for _, line := range lines[2:] {
		if len(line) > 0 {
			n := Node{}
			n.Parse(line)
			m[n.Input] = n

			if string(n.Input[2]) == "A" {
				positions = append(positions, n)
			}
		}
	}

	log.Printf("Positions: %#v", positions)

	// Suspecting some fixed loop sizes, try to find them
	sizes := make([]int, 0)
	for _, p := range positions {
		var arrived bool
		var i int
		var count int
		route := strings.Trim(lines[0], "\n")
		position := p
		for !arrived {
			if string(route[i]) == "L" {
				position = m[position.Left]
			} else {
				position = m[position.Right]
			}
			count++

			s := string(position.Input[2])
			if s == "Z" {
				arrived = true
			}

			i++
			if i == len(route) {
				i = 0
			}
		}

		sizes = append(sizes, count)
		log.Printf("Loop size for %#v: %d, end: %#v", p, count, position)
	}

	// Try to find an iteration where all loops align
	max := slices.Max[[]int, int](sizes)

	var done bool
	var result int
	for i := 1; !done; i++ {
		var fail bool
		for _, s := range sizes {
			if i*max%s != 0 {
				fail = true
			}
		}
		if !fail {
			result = i * max
			done = true
		}
	}
	return result, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 8)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day8Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day8Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
