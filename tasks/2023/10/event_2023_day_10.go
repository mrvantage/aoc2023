package main

import (
	"flag"
	"fmt"
	"log"
	"slices"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Tile struct {
	Value          string
	X              int
	Y              int
	MinPath        int
	MaxPath        int
	ConnectedTiles []*Tile
	Map            *Map
}

func (t *Tile) Connect() {
	m := *t.Map
	switch t.Value {
	case "F":
		if t.X+1 < len(m[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X+1])
		}
		if t.Y+1 < len(m) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y+1][t.X])
		}
	case "-":
		if t.X+1 < len(m[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X+1])
		}
		if t.X-1 >= 0 {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X-1])
		}
	case "7":
		if t.X-1 >= 0 {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X-1])
		}
		if t.Y+1 < len(m) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y+1][t.X])
		}
	case "|":
		if t.Y-1 >= 0 {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y-1][t.X])
		}

		if t.Y+1 < len(m) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y+1][t.X])
		}
	case "J":
		if t.Y-1 >= 0 {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y-1][t.X])
		}
		if t.X-1 >= 0 {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X-1])
		}
	case "L":
		if t.X+1 < len(m[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X+1])
		}
		if t.Y-1 >= 0 {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y-1][t.X])
		}
	case "S":
		if slices.Contains[[]byte, byte]([]byte("F|7"), []byte(m[t.Y-1][t.X].Value)[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y-1][t.X])
		}
		if slices.Contains[[]byte, byte]([]byte("7-J"), []byte(m[t.Y][t.X+1].Value)[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X+1])
		}
		if slices.Contains[[]byte, byte]([]byte("L|J"), []byte(m[t.Y+1][t.X].Value)[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y+1][t.X])
		}
		if slices.Contains[[]byte, byte]([]byte("F-L"), []byte(m[t.Y][t.X-1].Value)[0]) {
			t.ConnectedTiles = append(t.ConnectedTiles, m[t.Y][t.X-1])
		}
	}
}

func (t *Tile) Walk(pathLength int, source *Tile) {

	if pathLength > t.MaxPath {
		t.MaxPath = pathLength
	}

	if pathLength < t.MinPath || t.MinPath == -1 {
		t.MinPath = pathLength
	}

	if t.Value == "S" && pathLength > 0 {
		return
	}

	for _, c := range t.ConnectedTiles {
		if c != source {
			c.Walk(pathLength+1, t)
		}
	}
}

func (t Tile) Enhance(m *Map) (self, right, down, diagonal *Tile) {

	var rightVal, downVal string
	if slices.Contains[[]byte, byte]([]byte("F-L"), []byte(t.Value)[0]) {
		rightVal = "-"
	} else {
		rightVal = "."
	}

	if slices.Contains[[]byte, byte]([]byte("F|7"), []byte(t.Value)[0]) {
		downVal = "|"
	} else {
		downVal = "."
	}

	if t.Value == "S" {
		m := *t.Map
		nextValRight := m[t.Y][t.X+1].Value
		nextValDown := m[t.Y+1][t.X].Value

		if slices.Contains[[]byte, byte]([]byte("J-7"), []byte(nextValRight)[0]) {
			rightVal = "-"
		}

		if slices.Contains[[]byte, byte]([]byte("L|J"), []byte(nextValDown)[0]) {
			downVal = "|"
		}
	}

	self = &Tile{
		Value:          t.Value,
		X:              t.X * 2,
		Y:              t.Y * 2,
		MinPath:        -1,
		MaxPath:        -1,
		ConnectedTiles: make([]*Tile, 0),
		Map:            m,
	}

	right = &Tile{
		Value:          rightVal,
		X:              t.X*2 + 1,
		Y:              t.Y * 2,
		MinPath:        -1,
		MaxPath:        -1,
		ConnectedTiles: make([]*Tile, 0),
		Map:            m,
	}

	down = &Tile{
		Value:          downVal,
		X:              t.X * 2,
		Y:              t.Y*2 + 1,
		MinPath:        -1,
		MaxPath:        -1,
		ConnectedTiles: make([]*Tile, 0),
		Map:            m,
	}

	diagonal = &Tile{
		Value:          ".",
		X:              t.X*2 + 1,
		Y:              t.Y*2 + 1,
		MinPath:        -1,
		MaxPath:        -1,
		ConnectedTiles: make([]*Tile, 0),
		Map:            m,
	}
	return
}

func (t *Tile) Flood() {
	m := *t.Map
	t.Value = "O"
	t.MinPath = -2
	if t.X+1 < len(m[0]) && m[t.Y][t.X+1].MinPath == -1 {
		m[t.Y][t.X+1].Flood()
	}
	if t.Y+1 < len(m) && m[t.Y+1][t.X].MinPath == -1 {
		m[t.Y+1][t.X].Flood()
	}
	if t.X-1 >= 0 && m[t.Y][t.X-1].MinPath == -1 {
		m[t.Y][t.X-1].Flood()
	}
	if t.Y-1 >= 0 && m[t.Y-1][t.X].MinPath == -1 {
		m[t.Y-1][t.X].Flood()
	}
}

type Map [][]*Tile

func (m *Map) Parse(input []string) {
	mm := *m
	for y, row := range input {
		for x, val := range row {
			if string(val) != "\n" {
				t := Tile{
					Value:          string(val),
					X:              x,
					Y:              y,
					MinPath:        -1,
					MaxPath:        -1,
					ConnectedTiles: make([]*Tile, 0),
					Map:            m,
				}
				mm[y][x] = &t
			}
		}
	}
}

func (m Map) Connect() {
	for _, row := range m {
		for _, tile := range row {
			tile.Connect()
		}
	}
}

func (m Map) GetStart() (res *Tile) {
	for _, row := range m {
		for _, tile := range row {
			if tile.Value == "S" {
				res = tile
				return
			}
		}
	}
	return
}

func (m Map) StringValue() (res string) {
	res = "\n"
	for _, row := range m {
		for _, tile := range row {
			if tile.MinPath > -1 {
				res += tile.Value
			} else {
				res += "."
			}
		}
		res += "\n"
	}
	return
}

func (m Map) StringFloodedValue() (res string) {
	res = "\n"
	for _, row := range m {
		for _, tile := range row {
			if tile.MinPath > -1 || tile.MinPath < -1 {
				res += tile.Value
			} else {
				res += "."
			}
		}
		res += "\n"
	}
	return
}

func (m Map) String() (res string) {
	res = "\n"
	for _, row := range m {
		for _, tile := range row {
			res += tile.Value
		}
		res += "\n"
	}
	return
}

func (m Map) GetMaxShortestPath() (res int) {
	for _, row := range m {
		for _, tile := range row {
			if tile.MinPath > res {
				res = tile.MinPath
			}
		}
	}
	return
}

func (m Map) CountEnclosedPixels() (res int) {
	for _, row := range m {
		for _, tile := range row {
			if tile.MinPath == -1 {
				res++
			}
		}
	}
	return
}

func (m Map) Enhance() (res Map) {

	res = make(Map, len(m)*2)

	for i := range res {
		res[i] = make([]*Tile, len(m[0])*2)
	}

	for y, row := range m {
		for x, tile := range row {

			self, right, down, diagonal := tile.Enhance(&res)
			res[y*2][x*2] = self
			res[y*2][x*2+1] = right
			res[y*2+1][x*2] = down
			res[y*2+1][x*2+1] = diagonal
		}
	}
	return
}

func (m Map) DeEnhance() (res Map) {

	res = make(Map, len(m)/2)

	for i := range res {
		res[i] = make([]*Tile, len(m[0])/2)
	}

	for y, row := range m {

		if y%2 == 0 {
			for x, tile := range row {

				if x%2 == 0 {
					res[y/2][x/2] = &Tile{
						Value:          tile.Value,
						X:              x / 2,
						Y:              y / 2,
						MinPath:        tile.MinPath,
						MaxPath:        tile.MaxPath,
						ConnectedTiles: make([]*Tile, 0),
						Map:            &res,
					}
				}
			}
		}

	}
	return
}

func NewMap(input string) (res Map) {

	lines := strings.Split(input, "\n")

	res = make(Map, len(lines)-1)

	for i := range res {
		res[i] = make([]*Tile, len(lines[0]))
	}
	res.Parse(lines[0 : len(lines)-1])
	return
}

func Event2023Day10Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	m := NewMap(task.Input)
	log.Printf("%s", m)

	m.Connect()

	s := m.GetStart()

	s.Walk(0, nil)

	log.Printf("%s", m.StringValue())

	return m.GetMaxShortestPath(), nil
}

func Event2023Day10Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	// Note: still contains a bug when S is on input edge (as in example), too lazy to fix :-)

	m := NewMap(task.Input)
	em := m.Enhance()
	log.Printf("%s", em)

	em.Connect()

	s := em.GetStart()
	s.Walk(0, nil)

	log.Printf("%s", em.StringValue())

	em[0][0].Flood()

	log.Printf("%s", em)

	dem := em.DeEnhance()

	log.Printf("%s", dem.StringFloodedValue())

	return dem.CountEnclosedPixels(), nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 10)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day10Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day10Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
