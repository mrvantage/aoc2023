package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"slices"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
	"golang.org/x/exp/maps"
)

type Card byte

func (c Card) Value(jokered bool) (val int) {

	jackVal := 11
	if jokered {
		jackVal = 1
	}

	values := map[byte]int{
		[]byte("2")[0]: 2,
		[]byte("3")[0]: 3,
		[]byte("4")[0]: 4,
		[]byte("5")[0]: 5,
		[]byte("6")[0]: 6,
		[]byte("7")[0]: 7,
		[]byte("8")[0]: 8,
		[]byte("9")[0]: 9,
		[]byte("T")[0]: 10,
		[]byte("J")[0]: jackVal,
		[]byte("Q")[0]: 12,
		[]byte("K")[0]: 13,
		[]byte("A")[0]: 14,
	}
	val = values[byte(c)]
	return
}

type Hand struct {
	Cards []Card
	Bid   int
}

func (h *Hand) Parse(line string) (err error) {
	h.Cards = make([]Card, 5)
	parts := strings.Split(line, " ")

	h.Bid, err = strconv.Atoi(parts[1])
	if err != nil {
		return fmt.Errorf("unable to parse bid %s: %s", parts[1], err.Error())
	}

	for i, c := range parts[0] {
		h.Cards[i] = Card(c)
	}

	return
}

func (h Hand) String() (str string) {
	for _, c := range h.Cards {
		str += string(c)
	}

	str = fmt.Sprintf("%s %d", str, h.Bid)
	return
}

func (h Hand) countCards() (res map[Card]int, keys []Card) {
	res = make(map[Card]int)
	for _, c := range h.Cards {
		res[c]++
	}
	keys = maps.Keys(res)
	return
}

func (h Hand) IsFiveOfAKind() (res bool) {
	c, _ := h.countCards()
	if len(c) == 1 {
		res = true
	}
	return
}

func (h Hand) IsFourOfAKind() (res bool) {
	c, keys := h.countCards()
	if len(c) == 2 && (c[keys[0]] == 4 || c[keys[1]] == 4) {
		res = true
	}
	return
}

func (h Hand) IsFullHouse() (res bool) {
	c, keys := h.countCards()
	if len(c) == 2 && (c[keys[0]] == 3 || c[keys[1]] == 3) {
		res = true
	}
	return
}

func (h Hand) IsThreeOfAKind() (res bool) {
	c, keys := h.countCards()
	if len(c) == 3 && (c[keys[0]] == 3 || c[keys[1]] == 3 || c[keys[2]] == 3) {
		res = true
	}
	return
}

func (h Hand) IsTwoPair() (res bool) {
	c, keys := h.countCards()
	if len(c) == 3 && (c[keys[0]] == 2 || c[keys[1]] == 2 || c[keys[2]] == 2) && (c[keys[0]] == 1 || c[keys[1]] == 1 || c[keys[2]] == 1) {
		res = true
	}
	return
}

func (h Hand) IsOnePair() (res bool) {
	c, _ := h.countCards()
	if len(c) == 4 {
		res = true
	}
	return
}

func (h Hand) IsHighCard() (res bool) {
	c, _ := h.countCards()
	if len(c) == 5 {
		res = true
	}
	return
}

func (h Hand) ScoreType() (res int) {

	if h.IsFiveOfAKind() {
		res += 60000000000
	}
	if h.IsFourOfAKind() {
		res += 50000000000
	}
	if h.IsFullHouse() {
		res += 40000000000
	}
	if h.IsThreeOfAKind() {
		res += 30000000000
	}
	if h.IsTwoPair() {
		res += 20000000000
	}
	if h.IsOnePair() {
		res += 10000000000
	}

	return
}

func (h Hand) ScoreValue(jokered bool) (res int) {

	for i, c := range h.Cards {
		val := c.Value(jokered)
		// make sure spacing of factor is large enough (> factor 10)
		factor := int(math.Pow(10, float64((4-i)*2)))
		res += factor * val
	}
	return
}

func (h Hand) Score() (res int) {
	res += h.ScoreType() + h.ScoreValue(false)
	return
}

func (h Hand) ScoreWithJokerRule() (res int) {
	res += h.GetJokered().ScoreType() + h.ScoreValue(true)
	return
}

func (h *Hand) ApplyJoker() {

	cardCount, keys := h.countCards()

	// Joker should be replaced by value that we have most cards for, or highest ranked cards if same amount for multiple values
	targetCmp := func(a Card, b Card) int {
		// aStr := string(a)
		// bStr := string(b)
		// log.Printf("comparing %s with %s", aStr, bStr)

		if a == Card([]byte("J")[0]) {
			return -1
		} else if b == Card([]byte("J")[0]) {
			return 1
		} else if cardCount[a] > cardCount[b] {
			return 1
		} else if cardCount[a] < cardCount[b] {
			return -1
		} else if cardCount[a] == cardCount[b] && a.Value(false) >= b.Value(false) {
			return 1
		} else {
			return -1
		}
	}

	selectReplace := func() (res Card) {
		res = slices.MaxFunc[[]Card, Card](h.Cards, targetCmp)
		if res == Card([]byte("J")[0]) {
			res = Card([]byte("A")[0])
		}
		return
	}

	if slices.Contains[[]Card, Card](keys, Card([]byte("J")[0])) {

		replaceCard := selectReplace()

		for i, card := range h.Cards {
			if card == Card([]byte("J")[0]) {
				h.Cards[i] = replaceCard
			}
		}
	}
}

func (h Hand) GetJokered() (res Hand) {
	res.Cards = make([]Card, 0)
	res.Cards = append(res.Cards, h.Cards...)
	res.ApplyJoker()
	return
}

func Event2023Day7Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	hands := make([]Hand, 0)

	for _, line := range strings.Split(task.Input, "\n") {
		if len(line) > 0 {
			h := Hand{}
			h.Parse(line)
			hands = append(hands, h)
		}
	}

	slices.SortFunc[[]Hand, Hand](hands, func(a, b Hand) int {
		if a.Score() > b.Score() {
			return 1
		} else {
			return -1
		}
	})

	var res int
	for i, h := range hands {
		fmt.Printf("%s %d %d\n", h, h.Score(), h.Bid*(i+1))
		res += h.Bid * (i + 1)
	}

	return res, nil
}

func Event2023Day7Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	hands := make([]Hand, 0)

	for _, line := range strings.Split(task.Input, "\n") {
		if len(line) > 0 {
			h := Hand{}
			h.Parse(line)
			hands = append(hands, h)
		}
	}

	slices.SortFunc[[]Hand, Hand](hands, func(a, b Hand) int {
		if a.ScoreWithJokerRule() > b.ScoreWithJokerRule() {
			return 1
		} else {
			return -1
		}
	})

	var res int
	for i, h := range hands {
		inc := h.Bid * (i + 1)
		fmt.Printf("%s %d %s %d %d\n", h, h.Score(), h.GetJokered(), h.ScoreWithJokerRule(), inc)
		res += inc
	}

	return res, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2023, 7)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2023Day7Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2023Day7Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
